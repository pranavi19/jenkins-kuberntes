FROM tomcat:8

MAINTAINER 1010

COPY /target/mvn-hello-world.war /usr/local/tomcat/webapps/mvn-hello-world.war

EXPOSE 8080

USER root

WORKDIR /usr/share/tomcat/webapps   

CMD ["catalina.sh", "run"]